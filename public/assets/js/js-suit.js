const PLAYER = document.querySelectorAll(".player1");
const COMPUTER = document.querySelectorAll(".com");

let seletP1 = null;
let seletCOM = null;

function reset() {
    window.location.reload();
}

PLAYER.forEach(item => {
    item.addEventListener("click", () => {
        PLAYER[0].classList.remove("selected");
        PLAYER[1].classList.remove("selected");
        PLAYER[2].classList.remove("selected");

        item.classList.add("selected");
        const VALUE_SELECTED = item.getAttribute("value");
        showLogSelected("p1 " + VALUE_SELECTED);

        seletP1 = VALUE_SELECTED;
        seletCOM = getPilihanComputer();



        const arena = new Arena();
        arena.arenaRumble();
    })
});


// COMPUTER.forEach(item => {
//     console.log (item,"<< item")
//     item.addEventListener("click", () => {
//         COMPUTER[0].classList.remove("selected-com");
//         COMPUTER[1].classList.remove("selected-com");
//         COMPUTER[2].classList.remove("selected-com");

//         item.classList.add("selected-com");
//         const VALUE_SELECTED = item.getAttribute("value");
//         showLogSelected("com " + VALUE_SELECTED);

//         seletCOM = getPilihanComputer();

//         const arena = new Arena();
//         arena.arenaRumble();
//     })
// });



function showLogSelected(value) {
    console.log(`LOG_SELECTED : Ini Pilihan Pokemon Anda : ${value}`);
}



// Implementasi class
class Arena {
    arenaRumble() {
        if (seletP1 && seletCOM) {
            this.calculateScore(seletP1, seletCOM)
        // } else {
        //     console.log("LOG_ARENA : Masih ada yg belum memilih Pokemom!");
        }
    }

    //Aturan Permainan
    calculateScore(seletP1, seletCOM) {
        if (seletP1 === seletCOM) {
            this.showScore(null);
        } else if (seletP1 === "batu" && seletCOM === "gunting") {
            this.showScore("PLAYER 1");
        } else if (seletP1 === "batu" && seletCOM === "kertas") {
            this.showScore("COMPUTER");
        } else if (seletP1 === "gunting" && seletCOM === "batu") {
            this.showScore("COMPUTER");
        } else if (seletP1 === "kertas" && seletCOM === "batu") {
            this.showScore("PLAYER 1");
        } else if (seletP1 === "gunting" && seletCOM === "kertas") {
            this.showScore("PLAYER 1");
        } else if (seletP1 === "kertas" && seletCOM === "gunting") {
            this.showScore("COMPUTER");
        }
    }

    // Hasil Permainan
    showScore(winner) {
        // const suit_result = document.getElementById('win');
        // suit_result.innerHTML = "";
        document.getElementById('versus').style.visibility = "hidden";
        document.getElementById('win').style.visibility = "hidden";
        document.getElementById('lose').style.visibility = "hidden";
        document.getElementById('draw').style.visibility = "hidden";
        console.log(winner)
        if (winner == "PLAYER 1") {
            // suit_result.innerHTML= "Selamat <b>" + winner + "</b> Menang";
            document.getElementById('win').style.visibility = "visible";
            console.log("LOG_SCORE : "+ winner + "");
        } else if (winner == "COMPUTER"){
            // suit_result.innerHTML = "Seriii";
            document.getElementById('lose').style.visibility = "visible";
            console.log("LOG_SCORE : "+ winner + "");
        }else {
            document.getElementById('draw').style.visibility = "hidden";
            document.getElementById('draw').style.visibility = "visible";
            console.log("LOG_SCORE : Seriiii yaa!");
        }

        // suit_result.style.color = "black";
        // suit_result.style.visibility = "visible";
    }
}

//Pilihan Computer
function getPilihanComputer (){
    const comp = Math.random();
    document.querySelector('.com').style.backgroundColor = null;
    document.querySelector('.comkertas').style.backgroundColor = null;
    document.querySelector('.comgunting').style.backgroundColor = null;

    if (comp < 0.34) {
        const batucomKlik = document.querySelector('.com');
        // console.log (batucomKlik,"<< batu")
        batucomKlik.style.borderRadius = "25px";
        // batucomKlik.style.padding = "10px";
        batucomKlik.style.backgroundColor = '#C4C4C4';
        return 'batu';
    }
    else if (comp >= 0.34 && comp < 0.67) {
        const batucomKlik = document.querySelector('.comkertas');
        batucomKlik.style.backgroundColor = '#C4C4C4';
        return 'kertas';
    }
    else {
        const batucomKlik = document.querySelector('.comgunting');
        batucomKlik.style.backgroundColor = '#C4C4C4';
        return 'gunting';
    }

    }