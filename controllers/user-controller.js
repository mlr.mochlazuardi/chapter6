const { User } = require('./../models')

const userController = {
    get: (req, res) => {
        User.findAll().then((data) => {
            res.status(200).json({
                status: 'success',
                data: data,
            })
        })
    },
    get_by_id: (req, res) => {
        User.findOne({
            where: {
              id: req.params.id,
            },
          }).then((data) => {
            res.status(200).json({
              status: 'success',
              data: data,
            })
          })
    },
    create: (req, res) => {
            // 1. membaca data req [data request]
    console.log("MASUUK POST");

    // 2 pengelopokan body request
    // { ini isi dari req.body
    //   user_id: 'sdsd',
    //   username: 'sdsd',
    //   password: 'sdsd',
    // }
    const { user_id, email, username, password } = req.body

    // 3 menjalankan function querybuilder untuk query create data insert ke table
    // INSERT INTO "Users" ("id","user_id","username","password","createdAt","updatedAt") VALUES (DEFAULT,$1,$2,$3,$4,$5,$6) RETURNING "id","user_id","username","password","createdAt","updatedAt";
    User.create({
      user_id,
      email,
      username,
      password,
    })
      .then(() => {
        // res.status(201).json({
        //   status: 'success',
        //   data: req.body,
        //   message: 'Data User berhasil ditambahkan!',
        // })

        // const message = 'Data User berhasil ditambahkan!'
        // res.render('success', {
        //   message
        // })

        

        // 4 return success insert
        res.redirect("/user");
      })
      .catch((err) => {
        // 4 return gagal insert

        res.status(400).json({
          status: 'fail',
          data: req.body,
          message: err.message,
        })
      })
    },
    update: (req, res) => {
        const { user_id, email, username, password } = req.body
        User.update(
          {
            user_id,
            email,
            username,
            password,
          },
          {
            where: {
              id: req.params.id,
            },
          },
        )
          .then(() => {
            // res.status(202).json({
            //   status: 'success',
            //   data: req.body,
            //   message: 'Data User berhasil diubah!',
            // })
    
            // const message = 'Data User berhasil diubah!'
            // res.render('success', {
            //   message
            // })
    
            res.redirect("/user");
          })
          .catch((err) => {
            res.status(400).json({
              status: 'fail',
              data: req.body,
              message: err.message,
            })
          })
    },
    delete: (req, res) => {
        User.destroy({
            where: {
              id: req.params.id,
            },
          })
            .then(() => {
              // const message = 'Data User berhasil didelet!'
              // res.render('success', {
              //   message
              // })
      
              res.redirect("/user");
      
            })
            .catch((err) => {
              res.status(400).json({
                status: 'fail',
                message: err.message,
              })
            })
    },
}

module.exports = userController;