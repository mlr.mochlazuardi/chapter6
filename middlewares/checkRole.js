const checkRole = (role) => {
    return function (req, res, next) {
        console.log("masukrolee",req.user)
        if (req.isAuthenticated() && role.includes(req.user.roles.name)) {
            return next();
        }
        res.redirect('/login');
    }
}

module.exports = checkRole;