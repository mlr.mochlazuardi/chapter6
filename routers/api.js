const express = require("express");
const base_response = require("../libs/base-response");
const checkToken = require("../middlewares/checkToken");
const { Room } = require("../models");
const userController = require("../controllers/user-controller");
const apiHandlerNotFound = require("../middlewares/api-handler-not-found");
const user = express.Router();
const randomstring = require("randomstring");
const { waiting } = require("../libs/room-status");


user.get("/", userController.get);
user.get("/:id", userController.get_by_id);
user.post("/", userController.create);
user.post("/:id", userController.update);
user.get("/delete/:id", userController.delete);

user.post("/v1/create-room", checkToken, async (req, res, next) => {
    const roomInisiate = {
        kode : randomstring.generate({
            length: 5,
            charset: 'alphabetic'
        }).toUpperCase(),
        home : req.user.id,
        status: waiting
    }
    try {
        const rooms = await Room.create(roomInisiate)
        res.status(200).json(base_response(rooms, "success", "Create Room Berhasil!"))
        
    } catch (error) {
        res.status(400).json(null, "failed", error);
    }
})

// user.use(apiHandlerNotFound)
module.exports = user;