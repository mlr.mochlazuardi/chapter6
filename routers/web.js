const express = require('express');
const webHandlerNotFound = require("../middlewares/web-handler-not-found");
const webController = require("../controllers/webController")
const { User } = require("../models");
const checkRole = require('../middlewares/checkRole');
const web = express.Router();

// Chapter 7

web.get("/login", webController.index);
web.post('/login', webController.login);
web.get("/logout", webController.logout);
web.post("/registration", webController.registration)

// Login for role user, not admin
web.post("/auth/token", webController.loginToken);

// Chapter 6
web.get("/user", checkRole(["admin"]), (req, res, next) => {
    // 1. Proses Pertama ngejalanin function query builder dari model user findAll SELECT "id", "user_id", "username", "password", "createdAt", "updatedAt" FROM "users" AS "user"
    User.findAll().then((data) => {
        // 2. then (proses untuk retun data dari hasil query builder nya.. [employess] array user)

        // 3. Rendering dari file views/user.ejs

        // 4. Menyisipkan data array dari user ke halaman ejs.

        // 5 [di file ejs] di mejalankan embed script javascript yang disisipkan didalam user.ejs
        res.render("user", {
            users: data
        });
    });
});

web.get("/user/:id", checkRole(["admin"]), (req, res, next) => {
    // 1. baca request
    // 2.  run function query builder untuk findOne..
    // Executing (default): SELECT "id", "user_id", "username", "password", "email", "status", "createdAt", "updatedAt" FROM "users" AS "user" WHERE "user"."id" = '8';
    User.findOne({
        where : {
            id : req.params.id
        }
    }).then((data) => {

        // 3. return data sekaligus render halama view ejs
         // 4 [di file ejs] di mejalankan embed script javascript yang disisipkan didalam user.ejs
        res.render("user-detail", {
            user: data
        });
    })
});

web.get("/user-update/:id", checkRole(["admin"]), (req, res, next) => {
    User.findOne({
        where : {
            id : req.params.id
        }
    }).then((data) => {
        res.render("user-update", {
            user: data
        });
    })
})

// Chapter 5
web.get("/", (req, res) => {
    res.render("home", { user: req.isAuthenticated ? req.user: null })
});

web.get("/playnow", webController.playnow);

web.get("/signup", (req, res) => {
    res.render("signup", { account: "Ardi" });
});

// web.get("/login", (req, res) => {
//     res.render("login", { account: "Ardi" });
// });


// web.use(webHandlerNotFound);
module.exports = web;