const express = require('express');

// Routers
const api = require("./api");
const web = require("./web");
const login = require('./login');

// Setup Router
const router = express.Router();

console.log("MASUK RUTER")

// router.use("/login", login);
router.use("/users", api);
router.use("/web", web);

module.exports = router;