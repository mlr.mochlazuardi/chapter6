const express = require("express");
const morgan = require ("morgan");
const router = require("./routers/router");

const cookieParser = require('cookie-parser');
const session = require('express-session');
const flash = require('express-flash');
const passport = require("./libs/passport");

const passportJwt = require("./libs/passport-jwt");

const web = require("./routers/web");
const login = require("./routers/login");

const app = express();
const port = 3005;

// Middleware content-type
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

// proses inisasi middleware atuh
app.use(cookieParser());
app.use(flash());
app.use(session({
  secret: 'secretkey',
  resave: false,
  saveUninitialized: false
}));

// Local Strategy
app.use(passport.initialize());
app.use(passport.session());
// end proses inisiasi middleware.

// JWT
app.use(passportJwt.initialize());

// view engine setup
app.set("view engine", "ejs");
// app.set("views", "./views");

// Middleware Third Party
app.use(morgan("common"));

app.get("/ping", (req, res) => {
    res.send("Server run!");
})

// app.use("/ping", (req, res) => {
//     res.status(200).json({
//         status: "server up!"
//     })
// });

// config folder public
app.use(express.static("./public/assets"));
// app.use("/login", login);
app.use("/", web);
app.use("/api", router);



app.listen(port, () => {
    console.log(`Server running on server ${port}!`);
});

// module.exports = app;
// module.exports = router;
// module.exports = web;